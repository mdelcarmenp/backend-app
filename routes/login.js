const express = require('express');
const router = express.Router();
const login = require('../controllers/login');

router.post('/', function(req, res){
    login.index(req,res);
});

module.exports = router;
