const path = require('path');
const Cuenta = require('../models/cuentas'); // traer el módulo del modelo cuentas.js
const bcrypt = require('bcrypt');
var validator = require('validator');

/*
 * Comprueba el login
 * @param request del post en formato json
 * @param response del post en formato json o error
 *  
 *  Si el login es correcto retorna {"isuser": "True", "cuentaID": cuenta}
 *  Si el login es incorrecto retorna {"isuser": "False", "msg": 'Credenciales invalidas'}
 */
exports.index = function(req, res){
    var dni = req.body.dni;
    var clave = "" + req.body.clave;
     
    // Validar que el dni y la clave son entradas permitidas
    var  entradaValida = validator.isInt(dni) && dni > 0 && dni < 1000000000;
    entradaValida = entradaValida && clave.length <= 10 ;

    // Buscar al usuario y compara las claves
    Cuenta.find({"dni": dni, "habilitado": true},{"clave": 1, "habilitado": 1, "cuentaID": 1}).exec(function(err,doc){
        if(err){
	    console.log('Error al consultar la base ' +err);
	    return;
	}
        var msg = '{"isuser":"False", "msg" : "Credenciales inválidas"}';

        if(entradaValida && doc.length == 1){
            bcrypt.compare(clave,doc[0].clave).then(function(result) {
                if(result){
	            console.log('Credenciales validas');
	            res.json(JSON.parse('{"isuser":"True", "cuentaID": "'+doc[0].cuentaID+'"}'));
	        }else{
	            console.log('Credenciales invalidas');
	            res.json(JSON.parse(msg));
                }
	    });
        }else{
	    console.log('Credenciales invalidas');
	    res.json(JSON.parse(msg));
        }
    });
     
};
