const express = require('express')
const app = express()
const port = 4001

const db = require('./db'); // conexion a la base de datos del backend

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

const bodyParser = require('body-parser');

// habilitamos el acceso a los datos del post?
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', (req, res) => res.send('Hello World! API is Working'))

// Manejar post y get de cuentas
const cuentas = require('./routes/cuentas'); 
app.use('/cuentas', cuentas);

// Manejar post y get de movimientos
const movimientos = require('./routes/movimientos'); 
app.use('/movimientos', movimientos);

// Manejar post y get de logins
const login = require('./routes/login'); // modulo que maneja las rutas de post
app.use('/login', login);


// Escuchar en el puerto
app.listen(port, function () {
	 console.log('backend-app en http://localhost:'+port);
});
