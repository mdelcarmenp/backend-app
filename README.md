# Backend app in nodejs

The initial code is from [Polymer 3 starter kit with login using Node JS -Part 2](https://medium.com/@enghamzasalem77/polymer-3-starter-kit-with-login-using-node-js-part-2-acfcb8c83ce8)

### Setup

##### Prerequisites

Install [node.js](https://nodejs.org).
Install [mongodb](https://www.mongodb.com).

##### Setup mongodb
Setup a database user and password

##### Initialize project from globaldevtools repo
git clone ssh://git@globaldevtools.bbva.com:7999/practitioner/backend-app.git

cd backend-app
npm install

##### Setup mongo credentials
Change mongo credentials in db.js

##### Run app
node index.js
