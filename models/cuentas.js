const mongoose = require('mongoose');

// Definir Schema para la cuenta de usuario
//  con esto sabemos el tipo de entrada que esperamos
const Cuenta = mongoose.Schema({
        dni: {type: String, required: true},
        nombre: { type: String, required: true},
        email: {type: String, required: true},
        clave: {type: String, required: true},
        total: {type: Number, required: true},
        cuentaID: {type: String, required: true},
        habilitado: {type: Boolean, required: true},
	fecha: {type: Date, required:true}
});


// Dejar disponible el modelo como módulo para 
// poder usarlo en otro lugar de la aplicación
module.exports = mongoose.model('Cuenta', Cuenta);


