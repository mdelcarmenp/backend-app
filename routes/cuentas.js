const express = require('express');
const router = express.Router();
const cuenta = require('../controllers/cuentas');

router.get('/:cuenta', function(req, res){
    cuenta.index(req,res);
});

router.post('/:cuenta', function(req, res){
    console.log('PUT no funca, dejo POST /cuentas/:cuenta');
    cuenta.update(req,res);
});

router.get('/:cuenta/:operacion', function(req, res){
    console.log('GET /cuentas/:cuenta/:operacion');
    cuenta.operacion(req,res);
});

router.post('/', function(req, res) {
    cuenta.create(req,res);
});
// XXX Implementar
router.get('/get', function(req, res) {
    cuenta.list(req,res);
});

module.exports = router;
