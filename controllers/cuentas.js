const path = require('path');
const Cuenta = require('../models/cuentas'); // traer el módulo del modelo cuentas.js

var validator = require('validator');

var md5 = require('md5'); // para generar el hash de cuentaID
var bcrypt = require('bcrypt'); // para encriptar la clave
const BCRYPT_SALT_ROUNDS = 12;

// Funcion que trae una sola cuenta
exports.index = function(req, res){
        var cuentaID = req.params.cuenta;
        console.log(cuentaID);

        Cuenta.findOne({"cuentaID": cuentaID}).exec(function (err, cuenta) {
                if (err) {
                        console.log(err);
                        return res.send(500, err);
                }else{
                    console.log(cuenta);

		    if(cuenta && cuenta.habilitado == false){
			return res.send(500,err);
		    }
                    res.status(200).jsonp(cuenta);
                    //res.end();
                }
        });
};

// Funcion que trae solo los datos modificables
exports.operacion = function(req, res){
        var cuentaID = req.params.cuenta;
        console.log('operacion en ' +cuentaID);
        if(req.params.operacion == 'misdatos'){
            Cuenta.findOne({"cuentaID": cuentaID, "habilitado": true}).exec(function (err, cuenta) {
                if (err) {
                        console.log(err);
                        return res.send(500, err);
                }else{
		    console.log(cuenta);
                    res.status(200).jsonp({"nombre": cuenta.nombre, "email":cuenta.email, "dni": cuenta.dni});
		    
                }
            });
        } 
};

/*
 * Función para crear una cuenta
 * @ param request del post
 * @ param response en formato json
 *  
 *  response es { 'add': 'OK', 'isuser':'False' } si el ingreso fue correcto
 *  response es { 'add': msg, 'isuser':'False' } si el ingreso fue incorrecto
 *
 */
exports.create = function (req, res) {

  var c = req.body;

  // Validar datos de la entrada
  var entradaValida = validator.isEmail(''+c.email);
  entradaValida = entradaValida && validator.isInt(c.dni) && c.dni > 0 && c.dni < 1000000000; 
  entradaValida = entradaValida && c.nombre.length <= 50;
  if(!entradaValida){
         res.json(JSON.parse('{"add": "Algunos de los datos no son validos","isuser":"False"}'));
	 return
  }
  
  c.total = "0"; // inicializo la cuenta en 0
  c.habilitado = true; // marco la cuenta como habilitada
  c.fecha = new Date(); // Agrego el timestamp
  c.cuentaID = md5("" + c.dni + c.nombre + c.fecha); //invento una cuentaID

  Cuenta.find({"dni": c.dni},function (err, doc){
    if(err){
         var msg = "Erro en crear cuenta: al buscar dni de cuenta";
	 res.status(400).send(msg);
    }else if(doc.length >=1 ){
         res.json(JSON.parse('{"add": "Ya existe dni","isuser":"False"}'));
         console.log('Error en crear cuenta: el dni ya existe');
         res.end();
	 return;
    }
 
    // Encryptar la clave
    bcrypt.hash(c.clave, BCRYPT_SALT_ROUNDS, function(err, hashedPassword) {
        if(err){
            var msg = "Error en crear cuenta: al generar hash de la clave: " + err;
	    res.status(400).send(msg);
	    return;
	}
	
	c.clave = hashedPassword;

        var nuevaCuenta = new Cuenta(c);

        // Guardo la cuenta nueva
        nuevaCuenta.save(function (err) {
            if(err) {
	        var msg = "Erro en crear cuenta: No se pudo guardar la cuenta en la base";
	        console.log(msg);
	        console.log(err);
                res.status(400).send(msg);
            } else {
                res.json(JSON.parse('{"add": "OK","isuser":"True"}'));
                res.end();
	        console.log("Cuenta guardada");
            }
        });
    });
  });
};

exports.update = function (req, res) {
    var obj = req.body;
    
    var cuentaID = req.params.cuenta;
    // Segun https://mongoosejs.com/docs/2.7.x/docs/updating-documents.html
    Cuenta.findOne({"cuentaID": cuentaID, "habilitado": true},function (err, doc){
	console.log('Hago update con findOne :' + obj);
	var modificables = ['nombre', 'dni', 'email'];
	modificables.forEach(campo => {
	    if(obj.hasOwnProperty(campo)){
		doc[campo] = obj[campo];
		doc.save();
 	    }
	});
	if(obj.hasOwnProperty('baja')){
		if(doc['total'] == 0){
		    doc['habilitado'] = false;
		    doc.save();
		}else{
		     res.status(500).send("La cuenta debe estar en 0");
			return;
		}
 	}
	if(obj.hasOwnProperty('clave')){

            bcrypt.hash(obj.clave, BCRYPT_SALT_ROUNDS, function(err, hashedPassword) {
		  if(err){
                        console.log("Error guardando la clave: ");
			console.log(err);
		  }
		  console.log(hashedPassword);
		  doc.clave = hashedPassword;
		  doc.save();
                  //res.send();
              });
	}
	 res.status(200).jsonp({'update': 'OK'});
    });
};
// Listar el contenido
// llama a model.find() de mongoose
exports.list = function (req, res) {
        Cuenta.find({}).exec(function (err, cuentas) {
                if (err) {
			console.log(err);
                        return res.send(500, err);
                }
		console.log({"cuentas": cuentas});
        });
};

