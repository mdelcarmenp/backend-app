const mongoose = require('mongoose');

/*
 *  Schema para el movimiento de cuenta
 *  fuente: cuentaID de inicio 
 *  destino: cuentaID de destino 
 *  concepto: descripcion del movimiento
 *  monto: importe a depositar , extraer o transferir
 *  fecha: timestamp de la operación
 */
const Movimiento = mongoose.Schema({
        fuente: {type: String, required: false}, 
        destino: {type: String, required: false},
        concepto: {type: String, required: true},
        monto: {type: Number, required: true}, 
        fecha: {type: Date, required: true},
});

// Dejar disponible el modelo como módulo para 
// poder usarlo en otro lugar de la aplicación
module.exports = mongoose.model('Movimiento', Movimiento);


