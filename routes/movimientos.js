const express = require('express');
const router = express.Router();
const movimiento = require('../controllers/movimientos');

router.post('/', function(req, res){
    movimiento.list(req,res);
});

/* Agrega un movimiento de cuenta */
router.post('/agregar', function(req, res) {
    movimiento.create(req,res);
});

// XXX implementear
/* Listar los movimientos de la cuenta */
router.get('/', function(req, res) {
    movimiento.list(req,res);
});

module.exports = router;
