const path = require('path');
const Movimiento = require('../models/movimientos'); // traer el módulo del modelo movimientos.js
const Cuenta = require('../models/cuentas'); // traer el modulo de controlador de cuentas

// Funcion para mostar que la API esta escuchando
exports.index = function(req, res){
	res.send('/cuenta:  Hello World! API is Working')
};

function esTXValida (doc,tx){
    var monto = parseFloat(tx.monto);
    var esValida = tx.fuente != tx.destino && monto > 0;
    return esValida;
}

function responder(r,mensaje){
      r.json(JSON.parse('{"movimiento": "'+mensaje+'","isuser":"True"}'));
      r.end();
}
// Función para crear una entrada en la colección
//  Es llamada durante el POST del formulario
exports.create = function (req, res) {

    var tx = req.body;
    tx.fecha = new Date(); // Agregamos el timestap
    switch (tx.accion) {
         case "ingresar":
             tx.fuente = "";
             tx.destino = tx.cuentaID;
             break;
         case "extraer":
             tx.fuente = tx.cuentaID;
             tx.destino = "";
             break;
         case "transferir":
             tx.fuente = tx.cuentaID;
             tx.destino = tx.destino;
             break;
         default :
	     res.send("Datos mal ingresados");
	     res.end();
	     break;
    }
    var msg = "";

    // Comprobar que sea una tx válida, guardar el movimiento y modificar el total
    Cuenta.findOne({"cuentaID": tx.cuentaID, "habilitado": true}).exec(function(err,doc){
      if(err){
        msg = 'Error en buscar tx ';
	console.log(err);
	res.status(200);
	responder(res,msg);

      }else{
        if(esTXValida(doc,tx)){
	    var total = 0;
	    total = total + parseFloat(doc.total);

            if(tx.accion != "ingresar"){
		total = total -  parseFloat(tx.monto);
            }else{
	        total = total + parseFloat(tx.monto);
	    }
            if(total < 0){
		console.log('No hay saldo');
                responder(res,"No hay saldo suficiente");

	    }else{
                
                var nuevoMovimiento = new Movimiento(tx);
            
                console.log(nuevoMovimiento);

                // Guardar el movimiento en la base y el total en cuenta
                if(tx.accion == 'transferir'){
                    Cuenta.find({"cuentaID": tx.destino, "habilitado": true}).exec(function(err,doc_destino){
		        if(err){
		            msg = "Error al buscar cuenta destino";
			    res.status(200);
			    responder(res,msg);

			 }else{
				if(doc_destino.length > 0){
                                  nuevoMovimiento.save(function (err) {
                                    if(err) {
        	                      msg = "No se pudo guardar el movimiento en la base";
        	                      console.log(msg);
        	                      console.log(err);
                                      res.status(200);
			              responder(res,msg);

		                    }else{
				      // Saco tx.monto de la cuenta fuente
			              doc.total = total;
		                      doc.save();
				      // Le deposito a la cuenta destino
			              doc_destino[0].total = parseFloat(doc_destino[0].total) + parseFloat(tx.monto);
				      doc_destino[0].save();
			              responder(res,'OK');
                                    }
                                  });
				}else{
				     
				     msg = "No existe la cuenta destino";
				     console.log(msg);
				     responder(res,msg);
				}
			 }
		    });
		}else{
                  nuevoMovimiento.save(function (err) {
                    if(err) {
        	        msg = "No se pudo guardar el movimiento en la base";
        	        console.log(msg);
        	        console.log(err);
                        res.status(200);
			responder(res,msg);

		    }else{
			doc.total = total;
		        doc.save();
			responder(res,'OK');
                   }
                  });
	        }
	    }
        }else{
            console.log('Operacion invalida');
	    res.status(200).send("La cuenta no es correcta.");
	    res.end();
	    return;
        }
      }
    });
 
};

// Listar el contenido
// llama a model.find() de mongoose
exports.list = function (req, res) {
	var cuentaID = req.body.cuentaID;
	console.log(cuentaID);
	
        Movimiento.find({"$or":[ {"fuente": cuentaID}, {"destino": cuentaID}]}).sort({"fecha":-1}).exec(function (err, movimientos) {
                if (err) {
			console.log(err);
                        return res.send(500, err);
                }else{
		    res.status(200).jsonp(movimientos);
		}
        });
};

