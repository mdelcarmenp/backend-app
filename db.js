/* referencias:
 * -- https://www.digitalocean.com/community/tutorials/how-to-integrate-mongodb-with-your-node-application
 * -- https://mongoosejs.com/docs/deprecations.html
 */
const mongoose = require('mongoose'); // modulo para conectarse a mongo

// Informacion para conectarse a la base de sharks en mongo
const MONGO_USERNAME = 'dbadm';
const MONGO_PASSWORD = 'dbadm2019';
const MONGO_HOSTNAME = 'db';
const MONGO_PORT = '27017';
const MONGO_DB = 'UIAppDB';

// conectarse a la base
const options = {
  useNewUrlParser: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 500, 
  connectTimeoutMS: 10000,
};
const url = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`;

mongoose.connect(url, options).then( function() {
  console.log('MongoDB is connected');
})
  .catch( function(err) {
  console.log(err);
});

