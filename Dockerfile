FROM node:8.16.0

RUN mkdir -p /home/node/backend-app/node_modules && chown -R node:node /home/node/backend-app 

WORKDIR /home/node/backend-app

COPY package*.json ./

USER node

RUN npm install ; npm rebuild bcrypt --build-from-source

COPY --chown=node:node . .

EXPOSE 4001 

CMD [ "node", "index.js" ]
